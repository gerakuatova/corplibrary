package com.company;

import com.company.lesson.context.DataContext;
import com.company.lesson.context.InMemoryContext;
import com.company.lesson.lesson45.LessonServer;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        DataContext context = new InMemoryContext();
        try {
            new LessonServer("localhost", 8000, context).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
