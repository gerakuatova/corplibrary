package com.company.lesson.JsonHelper;

import com.company.lesson.library.BookDataModel;
import com.company.lesson.library.EmployeeDataModel;
import com.google.gson.Gson;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class JsonHelper <T> {

    private Gson gson;

    public JsonHelper() {
        this.gson = new Gson();
    }

    public  void writer(String path, T data) throws IOException {
        FileWriter writer = new FileWriter(path);
        String serializer = gson.toJson(data);
        writer.write(serializer);
        writer.close();

    }

    public BookDataModel readBooks(String path) throws IOException {
        FileReader reader = new FileReader(path);
        BookDataModel bookDataModel = gson.fromJson(reader, BookDataModel.class);
        reader.close();
        return bookDataModel;
    }


    public static EmployeeDataModel readEmployees(String path) throws IOException {
        Gson gson = new Gson();
        FileReader reader = new FileReader(path);
        EmployeeDataModel employeeDataModel = gson.fromJson(reader, EmployeeDataModel.class);
        reader.close();
        return employeeDataModel;
    }
}
