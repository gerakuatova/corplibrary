package com.company.lesson;

import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Utils {

    public static Map<String, String> parseUrlEncoded(String rawLines, String delimiter) {
        //email = test@mairl.ru&password=password123
        String[] pairs = rawLines.split(delimiter);

        Stream<Map.Entry<String, String>> stream =
                Arrays.stream(pairs)
                        .map(Utils::decode)
                        .filter(Optional::isPresent)
                        .map(Optional::get);

        //email - test@mail.ru
        return stream.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        //[
        // email - test@mail.ru
        // user password - password12
        // ]
    }

    static Optional<Map.Entry<String, String>> decode(String kv){
        if (!kv.contains("=")){
            return Optional.empty();
        }

        String[] pair = kv.split("="); //split email and password. this method checks it
        if (pair.length !=2){
            return Optional.empty(); //must be 2 elements like email = email@email.com - here 2 elements could be =mail.ru
        }
        Charset utf8 = StandardCharsets.UTF_8;
        String key = URLDecoder.decode(pair[0], utf8);
        String value = URLDecoder.decode(pair[1], utf8);

        return Optional.of(Map.entry(key, value));
        //email - test@email.com - now its written like this
    }
}
