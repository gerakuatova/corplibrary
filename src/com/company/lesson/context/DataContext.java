package com.company.lesson.context;

import com.company.lesson.library.Book;
import com.company.lesson.library.DomainUser;
import com.company.lesson.library.Employee;

import java.io.IOException;
import java.util.Optional;

public interface DataContext {
//    Optional<DomainUser> getUser(String email);
//    boolean addUser(DomainUser user);

    Optional<Employee> getUser(String email) throws IOException;
    boolean addUser(Employee user);



}
