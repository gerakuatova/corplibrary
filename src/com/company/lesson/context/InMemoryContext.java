package com.company.lesson.context;

import com.company.lesson.JsonHelper.JsonHelper;
import com.company.lesson.library.DomainUser;
import com.company.lesson.library.Employee;
import com.company.lesson.library.EmployeeDataModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class InMemoryContext implements DataContext{
//    ArrayList<DomainUser> users = new ArrayList<>();
//
//    public InMemoryContext() {
//        users.add(new DomainUser("test@mail.com", "Khanym", "123"));
//        users.add(new DomainUser("nurzhamal@mail.com", "Nurzhamal", "123"));
//        users.add(new DomainUser("aizhan@mail.com", "Aizhan", "123"));
//    }

    ArrayList<Employee> users = new ArrayList<>();

    public InMemoryContext() throws IOException {
        EmployeeDataModel bdm = JsonHelper.readEmployees("employees.json");
    }

    @Override
    public Optional<Employee> getUser(String email) throws IOException {
        EmployeeDataModel bdm = JsonHelper.readEmployees("employees.json");
        var employee = bdm.getEmployees()
                .stream()
                .filter(u -> u.getEmail().equals(email)).findFirst();
        return employee;
    }

    @Override
    public boolean addUser(Employee user) {
        return users.add(user);
    }


//    @Override
//    public Optional <DomainUser> getUser(String email) {
//        return users.stream().filter(u -> u.getEmail().equals(email)).findFirst();
//    }
//
//    @Override
//    public boolean addUser(DomainUser user) {
//        return users.add(user);
//    }
}
