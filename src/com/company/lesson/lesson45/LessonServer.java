package com.company.lesson.lesson45;

import com.company.lesson.JsonHelper.JsonHelper;
import com.company.lesson.Utils;
import com.company.lesson.context.DataContext;
import com.company.lesson.context.InMemoryContext;
import com.company.lesson.library.*;
import com.company.lesson.library.Error;
import com.company.lesson.server.RouteHandler;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import com.company.lesson.server.BasicServer;
import com.company.lesson.server.ContentType;
import com.company.lesson.server.ResponseCodes;


import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;

public class LessonServer extends BasicServer {
    private final static Configuration freemarker = initFreeMarker();
    private JsonHelper<BookDataModel> jsonHelperBooks = new JsonHelper<>();
    private JsonHelper<EmployeeDataModel> jsonHelperEmployees = new JsonHelper<>();
    private final DataContext dataContext;
    private Employee authorizedUser = new Employee("host@mail.ru", "gog", "123");
    private InMemoryContext context;

    public LessonServer(String host, int port, DataContext dataContext) throws IOException {
        super(host, port);
        this.dataContext = dataContext;
        this.context = new InMemoryContext();
        registerGet("/profile", this::profileHandler);
        registerGet("/books", this::booksHandler);
        registerGet("/employees", this::employeesHandler);
        registerGet("/register", this::registerGet);
        registerPost("/register", this::regPost);
        registerGet("/login", this::loginGet);
        registerPost("/login", this::loginPost);
        registerGet("/registrSuccess", this::regSuccess);
    }

    private void regSuccess(HttpExchange exchange) {
        Path path = makeFilePath("registrSuccess.html");
        sendFile(exchange, path, ContentType.TEXT_HTML);
    }

    private void profileHandler(HttpExchange exchange) {
        renderTemplate(exchange, "profile.html", authorizedUser);
    }

    private void registerGet(HttpExchange exchange) {
        Path path = makeFilePath("register.html");
        sendFile(exchange, path, ContentType.TEXT_HTML);
    }

    public void regPost(HttpExchange exchange) throws IOException {
        String raw = getBody(exchange);
        Map<String, String> parsed = Utils.parseUrlEncoded(raw, "&");
        Employee newUser = context.getUser(parsed.get("email")).orElse(null);
        if(newUser == null){
            var user = new Employee(parsed.get("email"), parsed.get("fullName"), parsed.get("user-password"));
            dataContext.addUser(user);

            redirect303(exchange, "/registrSuccess");
        }else {
            renderTemplate(exchange, "register.html", new Error("Регистрация не удалась"));
        }
    }


    private void loginGet(HttpExchange exchange) {
        Path path = makeFilePath("login.html");
        sendFile(exchange, path, ContentType.TEXT_HTML);
    }

    private void loginPost(HttpExchange exchange) throws IOException {
        String raw = getBody(exchange);
        Map<String, String> parsed = Utils.parseUrlEncoded(raw, "&");
        Employee userOpt = dataContext.getUser(parsed.get("email")).orElse(null);
        if (userOpt != null) {
            if(userOpt.getPassword().equals(parsed.get("user-password"))){
            authorizedUser = userOpt;
            redirect303(exchange, "/profile");
           }
        } else {
            redirect303(exchange, "/error");
        }
    }

    private void redirect303(HttpExchange exchange, String path) {
        try {
            exchange.getResponseHeaders().add("Location", path);
            exchange.sendResponseHeaders(303, 0);
            exchange.getResponseBody().close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private String getBody(HttpExchange exchange) {
        InputStream input = exchange.getRequestBody();
        InputStreamReader reader = new InputStreamReader(input, StandardCharsets.UTF_8);
        try(BufferedReader bufferedReader = new BufferedReader(reader)) {
            return bufferedReader.lines().collect(Collectors.joining(""));
        }catch (IOException e){
            e.printStackTrace();
        }
        return "";
    }

    protected void registerPost(String route, RouteHandler handler){
        registerGenericHandler("POST", route, handler);
    }

    private static Configuration initFreeMarker() {
        try {
            Configuration cfg = new Configuration(Configuration.VERSION_2_3_29);
            cfg.setDirectoryForTemplateLoading(new File("data"));
            cfg.setDefaultEncoding("UTF-8");
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            cfg.setLogTemplateExceptions(false);
            cfg.setWrapUncheckedExceptions(true);
            cfg.setFallbackOnNullLoopVariable(false);
            return cfg;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String getQueryParams(HttpExchange exchange) {
        String queryParams = exchange.getRequestURI().getQuery();
        return Objects.nonNull(queryParams) ? queryParams : "";
    }

    private void booksHandler(HttpExchange exchange) throws IOException {
        BookDataModel bdm = jsonHelperBooks.readBooks("books.json");
        String queryParams = getQueryParams(exchange);
        Map<String, String> params = Utils.parseUrlEncoded(queryParams, "&");
        String id = params.get("id");
        Book book = null;
        if (id != null){
            book = bdm.getBooks()
                    .stream()
                    .filter(b -> b.getId() == Integer.parseInt(id))
                    .findAny()
                    .orElse(null);
        }
        if (book == null){
            renderBookTemplate(exchange, "books.html", bdm);

        }else {
            renderTemplate(exchange, "book.html", book);
        }
    }

    private void employeesHandler(HttpExchange exchange) throws IOException {
        EmployeeDataModel bdm = JsonHelper.readEmployees("employees.json");
        String queryParams = getQueryParams(exchange);
        Map<String, String> params = Utils.parseUrlEncoded(queryParams, "&");
        String id = params.get("id");
        Employee employee = null;
        if (id != null){
            employee = bdm.getEmployees()
                    .stream()
                    .filter(b -> b.getId() == Integer.parseInt(id))
                    .findAny()
                    .orElse(null);
        }
        if (employee == null){
            renderBookTemplate(exchange, "employees.html", bdm);

        }else {
            renderTemplate(exchange, "employee.html", employee);
        }
    }

    protected void renderBookTemplate(HttpExchange exchange, String templateFile, Object dataModel) {
        try {
            Template temp = freemarker.getTemplate(templateFile);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            try (OutputStreamWriter writer = new OutputStreamWriter(stream)) {
                temp.process(dataModel, writer);
                writer.flush();
                var data = stream.toByteArray();
                sendByteData(exchange, ResponseCodes.OK, ContentType.TEXT_HTML, data);
            }
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

    protected void renderTemplate(HttpExchange exchange, String templateFile, Object dataModel) {
        try {
            Template temp = freemarker.getTemplate(templateFile);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            try (OutputStreamWriter writer = new OutputStreamWriter(stream)) {
                temp.process(dataModel, writer);
                writer.flush();
                var data = stream.toByteArray();
                sendByteData(exchange, ResponseCodes.OK, ContentType.TEXT_HTML, data);
            }
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

    private EmployeeDataModel getEmployeeDataModel() throws IOException {
        return  JsonHelper.readEmployees("employees.json");
    }
}
