package com.company.lesson.lesson47;

import com.company.lesson.Utils;
import com.company.lesson.context.DataContext;
import com.company.lesson.lesson45.LessonServer;
import com.company.lesson.library.Book;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Lesson47Server extends LessonServer {

    public Lesson47Server(String host, int port, DataContext dataContext) throws IOException {
        super(host, port, dataContext);
        //registerGet("/query", this::handleQueryRequest);
    }
//
//    private void handleQueryRequest(HttpExchange exchange){
//        String queryParams = getQueryParams(exchange);
//        Map<String, String> params = Utils.parseUrlEncoded(queryParams, "&");
//        Map<String, Object> data = new HashMap<>();
//        data.put("params", params);
//        renderTemplate(exchange, "query.html", data);
//    }
//
//    private String getQueryParams(HttpExchange exchange) {
//        String queryParams = exchange.getRequestURI().getQuery();
//        return Objects.nonNull(queryParams) ? queryParams : "";
//    }
}
