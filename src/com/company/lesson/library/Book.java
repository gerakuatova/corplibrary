package com.company.lesson.library;

public class Book {
    private int id;
    private String title;
    private String author;
    private String img;
    private boolean isAvailable;
    Employee employee;

    public Book(int id, String title, String author) {
        this.id = id;
        this.title = title;
        this.author = author;

    }

    public Book(int id, String title, String author, String img) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.img = img;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public boolean isAvailable() {

        return isAvailable;
    }

    public void setAvailable(boolean available) {
        isAvailable = available;
    }

    @Override
    public String toString() {
        return
                title + ", " + author;
    }
}
