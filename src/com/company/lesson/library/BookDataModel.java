package com.company.lesson.library;

import java.util.ArrayList;
import java.util.List;

public class BookDataModel {

    private Book book;
    private List<Book> books = new ArrayList<>();

    public BookDataModel() {
//        books.add(new Book(1, "Пляж", "А.Гарленд", "/images/theBeach.jpeg"));
//        books.add(new Book(2, "Волшебная Долина", "Й. Спири"));
//        books.add(new Book(3, "Земля", "Перл Бак"));
//        books.add(new Book(4, "Гарри Поттер и философский камень", "Р.Дж.Роулинг"));
//        books.add(new Book(5, "Гарри Поттер и тайная комната", "Р.Дж.Роулинг"));
//        books.add(new Book(6, "Гарри Поттер и узник Азкабана", "Р.Дж.Роулинг"));
//        books.add(new Book(7, "Гарри Поттер и Кубок Огня", "Р.Дж.Роулинг"));
//        books.add(new Book(8, "Гарри Поттер и Принц Полкукровка", "Р.Дж.Роулинг"));
//        books.add(new Book(9, "Гарри Поттер и Орден Феникса", "Р.Дж.Роулинг"));
//        books.add(new Book(10, "Гарри Поттер и Дары Смерти", "Р.Дж.Роулинг"));
//        books.add(new Book(11, "Гостья", "М"));
//        books.add(new Book(12, "Мрачный Жнец", "Т. Пратчетт"));
//        books.add(new Book(13, "Ночная смена", "С. Кинг"));
//        books.add(new Book(14, "Все", "Э.А. По"));
//        books.get(1).setAvailable(true);
//        books.get(5).setAvailable(true);
//        books.get(8).setAvailable(true);
    }

//    public Book getBook() {
//        book = books.get(1);
//        return book;
//    }

    public void setBook(Book book) {
        this.book = book;
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }


}
