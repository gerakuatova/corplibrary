package com.company.lesson.library;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Employee {
    private int id;
    private String fullName;
    private int amount;
    private List<Book> booksCur;
    private List<Book> bookPrevious;
    private String email;
    private String password;

    public Employee(String fullName, int amount, List<Book> booksCur) {
        this.fullName = fullName;
        this.amount = amount;
        this.booksCur = booksCur;
    }

    public Employee(String fullName, String email, String password) {
        this.fullName = fullName;
        this.email = email;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public List <Book> getBookPrevious() {
        return bookPrevious;
    }

    public List<Book> getBooksCur() {
        return booksCur;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", fullName='" + fullName + '\'' +
                ", amount=" + amount +
                ", booksCur=" + booksCur +
                ", bookPrevious=" + bookPrevious +
                '}';
    }
}

