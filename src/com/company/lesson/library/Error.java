package com.company.lesson.library;

public class Error {
    private String msg;

    public Error(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
